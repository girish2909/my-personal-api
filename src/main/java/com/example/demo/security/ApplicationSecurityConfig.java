package com.example.demo.security;

import static com.example.demo.enums.ApplicationUserRole.GENERAL;
import static com.example.demo.enums.ApplicationUserRole.ONLYREAD;
import static com.example.demo.enums.ApplicationUserRole.READWRITE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter{

	//NOTE : we can initialized any class level final variable using constructor initialization.
	private final PasswordEncoder passwordEncoder;


	//NOTE : we have initialized final variable using constructor overloading.
	@Autowired
	public ApplicationSecurityConfig(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http
		.csrf().disable()
		.authorizeHttpRequests()
		//Given below line is used for whitelist of project urls
		// Order of antMatchers for permission or role based authentication matters.
		.antMatchers("/","index","/css/*","/js/*").permitAll()
		.antMatchers("/api/**").hasRole(GENERAL.name() ) // ROLE BASED AUTHENTICATION
//		.antMatchers(HttpMethod.DELETE,"/management/api/**").hasAuthority(COURSE_WRITE.getPermission())
//		.antMatchers(HttpMethod.PUT,"/management/api/**").hasAuthority(COURSE_WRITE.getPermission())
//		.antMatchers(HttpMethod.POST,"/management/api/**").hasAuthority(COURSE_WRITE.getPermission())
//		.antMatchers(HttpMethod.GET,"/management/api/**").hasAnyRole(READWRITE.name(),ONLYREAD.name())
		.anyRequest()
		.authenticated()
		.and()
		//.formLogin()
		.httpBasic();
	}

	@Override
	@Bean
	protected UserDetailsService userDetailsService() {
 
		UserDetails allRoleUser = User.builder()
				.username("normal")
				.password(passwordEncoder.encode("password"))
				// .roles(GENERAL.name())
				.authorities(GENERAL.getGrantedAuthorities())
				.build();
		UserDetails admin = User.builder()
				.username("admin")
				.password(passwordEncoder.encode("password123"))
				// .roles(READWRITE.name())
				.authorities(READWRITE.getGrantedAuthorities())
				.build();
		UserDetails tom = User.builder()
				.username("tom")
				.password(passwordEncoder.encode("password123"))
				// .roles(ONLYREAD.name())
				.authorities(ONLYREAD.getGrantedAuthorities())
				.build();
		// TODO Auto-generated method stub
		return new InMemoryUserDetailsManager(allRoleUser,admin,tom);
	}

} 
