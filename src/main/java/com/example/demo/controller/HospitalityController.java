package com.example.demo.controller;

import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/hospitality")
public class HospitalityController {
	
	@GetMapping("/pathparam")
	public String testPathParam(@PathParam(value = "abc")	 String name) {
		//  /v1/hospitality/pathparam?name=girish
		// Attribute is mandatory
		return "This is path param where name is => "+name;
		
	}
	
	@GetMapping("/pathvariable/{name}")
	public String testPathVariable(@PathVariable String name) { 
		// /v1/hospitality/pathvariable/girish
		// Attribute not mandatory
		return "This is path variable where name is => "+name;
	}
	
	@GetMapping("/requestparam")
	public String testRequestParam(@RequestParam String name) {
		// /v1/hospitality/requestparam?name=girish
		// Attribute not mandatory and provide default value of and argument
		return "This is request param where name is => "+name; 
	}
}
